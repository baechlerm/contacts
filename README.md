# Contacts

Application développé durant le projet de semestre 6 à la HEIA-FR.

### Description

Le but de l’application Contacts est de permettre à l’utilisateur de créer ses propres cartes de visite avec les informations qu’il choisit lui-même et de les partager avec les autres utilisateurs. Lorsqu’un autre utilisateur reçoit la carte de visite du premier, son compte sauvegarde les informations et lui permet de les visualiser. Lorsque celui à qui appartient la carte de visite change, par exemple, de numéro de téléphone ou d’adresse, il n’a qu’à modifier sa carte de visite pour que les personnes la possédant reçoivent les nouvelles données à jour.

### Objectifs/Tâches

Le projet se divise en plusieurs étapes :

* Analyse et compréhension du kit de développement Flutter.
* Analyse et compréhension de l'outil Firebase de Google.
* Développement d'une gestion d'utilisateur avec Firebase Authentication.
* Développement d’un module de base de données avec Firestore.
* Développement de l'application multi-plateforme.

### Contributions

Ce projet est réalisé par l'étudiant Mathieu Baechler et est sous la supervision de :

* Professeur : Pascal Bruegger
* Co-superviseur : Nicolas Zurbuchen

### Source

Ces informations proviennent d'un projet réalisé à la Haute école d'ingénierie et d'architecture Fribourg lors de l'année 2019-2020.
