import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/FirebaseDatabase.dart';
import 'package:u_contact/ui/RootPage.dart';

import 'bloc/BlocProvider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DatabaseProvider(
      data: FirebaseDatabase(),
      child: BlocProvider<AccountBloc>(
        bloc: AccountBloc(),
        child: MaterialApp(
          title: 'uContact',
          theme: themeInit(),
          home: RootPage(),
        ),
      )
    );
  }

  ThemeData themeInit() {
    return ThemeData(
      // Color general
      primaryColor: Color.fromRGBO(0x2e, 0x7d, 0x32, 1),
      primaryColorDark: Color.fromRGBO(0x1b, 0x5e, 0x20, 1),
      primaryColorLight: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
      primaryColorBrightness: Brightness.dark,
      dividerColor: Colors.transparent,
      accentColor: Color.fromRGBO(0x2e, 0x7d, 0x32, 1),

      // Floating button
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Color.fromRGBO(0x66, 0xbb, 0x6a, 1),
        foregroundColor: Color.fromRGBO(0x33, 0x33, 0x33, 1),
        splashColor: Color.fromRGBO(0x4c, 0xaf, 0x50, 1),
      ),

      // Button
      buttonTheme: ButtonThemeData(
        buttonColor: Color.fromRGBO(0x81, 0xC7, 0x84, 1.0),
        disabledColor: Color.fromRGBO(0xb1, 0xd7, 0xb4, 1.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),

      // Card
      cardTheme: CardTheme(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  static InputDecoration getInputDecoration(String hintText) {
    return InputDecoration(
      contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),

      filled: true,
      fillColor: Color.fromRGBO(0xE8, 0xF5, 0xE9, 1),

      border: InputBorder.none,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(50.0)),
        borderSide: BorderSide(color: Color(00000000)),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(50.0)),
        borderSide: BorderSide(color: Color(00000000)),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(50.0)),
        borderSide: BorderSide(color: Color(00000000)),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(50.0)),
        borderSide: BorderSide(color: Color(00000000)),
      ),
      hintText: hintText,
    );
  }
}