import 'package:flutter/material.dart';
import 'Bloc.dart';

class BlocProvider<T extends Bloc> extends InheritedWidget {
  final T bloc;

  BlocProvider({
    this.bloc,
    Widget child,
    Key key,
  }) : super(
    child: child,
    key: key,
  );

  static BlocProvider<T> of<T extends Bloc>(BuildContext context) => context.dependOnInheritedWidgetOfExactType<BlocProvider<T>>();

  @override
  bool updateShouldNotify(BlocProvider oldWidget) {
    return false;
  }
}