import 'package:flutter/material.dart';
import 'package:u_contact/data/IDatabase.dart';

class DatabaseProvider extends InheritedWidget {
  final IDatabase data;

  DatabaseProvider({
    this.data,
    Widget child,
    Key key,
  }) : super(
    child: child,
    key: key,
  );

  static DatabaseProvider of(BuildContext context) => context.dependOnInheritedWidgetOfExactType<DatabaseProvider>();

  @override
  bool updateShouldNotify(DatabaseProvider oldWidget) {
    return false;
  }
}