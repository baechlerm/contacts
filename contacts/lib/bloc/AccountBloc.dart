import 'dart:async';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/data/model/User.dart';
import 'package:u_contact/ui/application/pages/ShareProfilesPage.dart';

import 'Bloc.dart';

class AccountBloc implements Bloc {
  static final AccountBloc _accountBloc = AccountBloc._internal();
  factory AccountBloc() {
    return _accountBloc;
  }
  AccountBloc._internal();

  // User
  User _user;
  User get connectedUser => _user;

  // Contacts
  List<Contact> _contacts = List<Contact>();
  List<Contact> _localContacts = List<Contact>();
  List<Contact> get allContacts {
    List<Contact> contacts = List<Contact>();
    contacts.addAll(_contacts);
    contacts.addAll(_localContacts);
    return contacts;
  }

  // Profiles
  List<Profile> _profiles = List<Profile>();
  List<Profile> get userProfiles => _profiles;

  // StreamControllers
  final _userController     = StreamController<User>.broadcast();
  final _contactsController = StreamController<List<Contact>>.broadcast();
  final _profilesController = StreamController<List<Profile>>.broadcast();

  // Getter streams
  Stream<User>          get userStream     => _userController.stream;
  Stream<List<Contact>> get contactsStream => _contactsController.stream;
  Stream<List<Profile>> get profilesStream => _profilesController.stream;

  // Update user
  void userRetrieve(User user) {
    _user = user;
    _userController.sink.add(user);
  }

  // Update contact
  void localContactsRetrieve(List<Contact> contacts) {
    _localContacts = contacts;
    _contactsController.sink.add(allContacts);
  }
  void contactsRetrieve(List<Contact> contacts) {
    _contacts = contacts;
    _contactsController.sink.add(allContacts);
  }
  void addLocalContact(Contact contact) {
    _localContacts.add(contact);
    _contactsController.sink.add(allContacts);
  }
  void deleteLocalContact(Contact contact) {
    Contact toDelete;
    _localContacts.forEach((cont) {
      if (cont.identifier == contact.identifier) {
        toDelete = cont;
      }
    });
    _localContacts.remove(toDelete);
    _contactsController.sink.add(allContacts);
  }
  void addContact(Contact contact) {
    _contacts.add(contact);
    connectedUser.contacts.add(contact);
    _contactsController.sink.add(allContacts);
  }
  void deleteContact(Contact contact) {
    Contact toDelete;
    _contacts.forEach((cont) {
      if (cont.profileId == contact.profileId) {
        toDelete = cont;
      }
    });
    _contacts.remove(toDelete);
    _contactsController.sink.add(allContacts);
  }
  void updateContact(Contact contact) {
    Contact toDelete;
    _contacts.forEach((cont) {
      if (cont.profileId == contact.profileId) {
        toDelete = cont;
      }
    });
    _contacts.remove(toDelete);
    _contacts.add(contact);
    _contactsController.sink.add(allContacts);
  }

  // Update profile
  void profilesRetrieve(List<Profile> profiles) {
    _profiles = profiles;
    _profilesController.sink.add(userProfiles);
  }
  void addProfile(Profile profile) {
    ShareProfilesPage.indexTab++;
    _profiles.insert(ShareProfilesPage.indexTab, profile);
    _profilesController.sink.add(userProfiles);
  }
  void updateProfile(Profile profile) {
    Profile toDelete;
    userProfiles.forEach((pro) {
      if (pro.id == profile.id) {
        toDelete = pro;
      }
    });
    _profiles.remove(toDelete);
    _profiles.insert(ShareProfilesPage.indexTab, profile);
    _profilesController.sink.add(userProfiles);
  }
  void deleteProfile(Profile profile) {
    Profile toDelete;
    userProfiles.forEach((pro) {
      if (pro.id == profile.id) {
        toDelete = pro;
      }
    });
    _profiles.remove(toDelete);
    _profilesController.sink.add(userProfiles);
  }

  @override
  void dispose() {
    _contactsController.close();
    _profilesController.close();
    _userController.close();
  }
}