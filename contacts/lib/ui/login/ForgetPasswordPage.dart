import 'package:flutter/material.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';

class ForgetPasswordPage extends StatefulWidget {
  _ForgetPasswordPageState createState() {
    return _ForgetPasswordPageState();
  }
}


class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _error = "";
  bool _isWaiting = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Color.fromRGBO(0x1b, 0x5e, 0x20, 1), //change your color here
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logo_maximize.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30, 140, 30, 140),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  _error,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
                formForgetWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget formForgetWidget() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            maxLines: 1,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Email',
            ),
            validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
            onSaved: (value) => _email = value.trim(),
          ),
          Container(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text(
                    'Send email'
                ),
                onPressed: onForgotPasswordPressed,
              ),
              showWaitingIndicator(),
            ],
          ),
        ],
      ),
    );
  }

  Widget showWaitingIndicator() {
    if (_isWaiting) {
      return Container(
        padding: EdgeInsets.all(10),
        width: 40,
        height: 40,
        child: CircularProgressIndicator(),
      );
    }

    return Container();
  }

  void onForgotPasswordPressed() async {
    final form = _formKey.currentState;
    final database = DatabaseProvider.of(context).data;

    if (form.validate()) {
      form.save();
      setState(() {
        _isWaiting = true;
      });

      try {
        await database.forgetPassword(_email);
        Navigator.pop(context);
      } catch (e) {
        setState(() {
          _formKey.currentState.reset();
          _error = e.message;
          _isWaiting = false;
        });
      }
    } else {
      setState(() {
        _error = "";
      });
    }
  }
}