import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/model/User.dart';
import 'package:u_contact/ui/login/ForgetPasswordPage.dart';

import '../RootPage.dart';

class SignInPage extends StatefulWidget {
  _SignInPageState createState() {
    return _SignInPageState();
  }
}

class _SignInPageState extends State<SignInPage> {
  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  String _error = "";
  bool _isWaiting = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Color.fromRGBO(0x1b, 0x5e, 0x20, 1), //change your color here
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logo_maximize.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30, 140, 30, 140),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  _error,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
                formSignInWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget formSignInWidget() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            maxLines: 1,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Email',
            ),
            validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
            onSaved: (value) => _email = value.trim(),
          ),
          TextFormField(
            maxLines: 1,
            obscureText: true,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Password',
            ),
            validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
            onSaved: (value) => _password = value.trim(),
          ),
          FlatButton(
            child: Text('Forgot password?'),
            onPressed: onForgotPasswordPressed,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text(
                    'Sign in'
                ),
                onPressed: onSignIn,
              ),
              showWaitingIndicator(),
            ],
          ),
        ],
      ),
    );
  }

  Widget showWaitingIndicator() {
    if (_isWaiting) {
      return Container(
        padding: EdgeInsets.all(10),
        width: 40,
        height: 40,
        child: CircularProgressIndicator(),
      );
    }

    return Container();
  }

  void onSignIn() async {
    final form = _formKey.currentState;
    final database = DatabaseProvider.of(context).data;

    if (form.validate()) {
      form.save();
      setState(() {
        _isWaiting = true;
      });

      try {
        User user = await database.signIn(_email, _password);
        BlocProvider.of<AccountBloc>(context).bloc.userRetrieve(user);
      } catch (e) {
        setState(() {
          _formKey.currentState.reset();
          _error = e.message;
          _isWaiting = false;
        });
      }
    } else {
      setState(() {
        _error = "";
      });
    }
  }

  void onForgotPasswordPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ForgetPasswordPage()),
    );
  }
}