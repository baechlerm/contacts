import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/model/User.dart';

import '../RootPage.dart';

class SignUpPage extends StatefulWidget {
  _SignUpPageState createState() {
    return _SignUpPageState();
  }
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = new GlobalKey<FormState>();
  final TextEditingController _currentPassword = TextEditingController();

  String _email;
  String _password;
  String _error = "";
  String _name = "";
  bool _isWaiting = false;
  String _errorCheckBox = "";
  bool _hasReadTerms   = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Color.fromRGBO(0x1b, 0x5e, 0x20, 1), //change your color here
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logo_maximize.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30, 140, 30, 140),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  _error,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
                formSignUpWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }



  Widget formSignUpWidget() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Name',
            ),
            validator: (value) => value.isEmpty ? 'Name can\'t be empty' : null,
            onSaved: (value) => _name = value.trim(),
          ),
          TextFormField(
            maxLines: 1,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Email',
            ),
            validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
            onSaved: (value) => _email = value.trim(),
          ),
          TextFormField(
            maxLines: 1,
            obscureText: true,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Password',
            ),
            controller: _currentPassword,
            validator: (value) {
              if (value.isEmpty)
                return 'Password can\'t be empty';
              if (value.length < 6)
                return 'Password must be 6 characters long';
              return null;
            },
            onSaved: (value) => _password = value.trim(),
          ),
          TextFormField(
            maxLines: 1,
            obscureText: true,
            autofocus: false,
            decoration: new InputDecoration(
              hintText: 'Password confirmation',
            ),
            validator: (value) =>
            _currentPassword.text != value
                ? 'Both passwords must be the same'
                : null,
          ),
          FlatButton(
            padding: EdgeInsets.all(0),
            onPressed: () {
              setState(() {
                _hasReadTerms = !_hasReadTerms;
                if (_hasReadTerms) {
                  _errorCheckBox = "";
                }
              });
            },
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Checkbox(
                  onChanged: null,
                  value: _hasReadTerms,
                ),
                Text('Confirm '),
                FlatButton(
                  textColor: Color.fromRGBO(0x00, 0xC8, 0x53, 1.0),
                  padding: EdgeInsets.all(0),
                  child: Text('Terms & Conditions'),
                  onPressed: onTermsPressed,
                ),
                Text(' read'),
              ],
            ),
          ),
          errorCheckboxWidget(),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text(
                    'Sign up'
                ),
                onPressed: onSignUp,
              ),
              showWaitingIndicator(),
            ],
          ),
        ],
      ),
    );
  }

  void onTermsPressed() {
    // TODO
  }

  Widget errorCheckboxWidget() {
    if (_errorCheckBox.length > 0) {
      return Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              _errorCheckBox,
              style: TextStyle(
                color: Colors.red,
              ),
            ),
          )
      );
    }

    return Container(
      height: 0.0,
    );
  }

  void onSignUp() async {
    if (!_hasReadTerms) {
      setState(() {
        _errorCheckBox = "You must agree with the Terms & Conditions";
      });
    } else {
      final form = _formKey.currentState;
      final database = DatabaseProvider.of(context).data;

      if (form.validate()) {
        form.save();
        setState(() {
          _isWaiting = true;
        });

        try {
          User user = await database.signUp(_email, _password, _name);
          BlocProvider.of<AccountBloc>(context).bloc.userRetrieve(user);
        } catch (e) {
          setState(() {
            _formKey.currentState.reset();
            _error = e.message;
            _isWaiting = false;
          });
        }
      } else {
        setState(() {
          _error = "";
        });
      }
    }
  }

  Widget showWaitingIndicator() {
    if (_isWaiting) {
      return Container(
        padding: EdgeInsets.all(10),
        width: 40,
        height: 40,
        child: CircularProgressIndicator(),
      );
    }

    return Container();
  }
}