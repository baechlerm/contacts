import 'package:flutter/material.dart';
import 'package:u_contact/ui/login/SignInPage.dart';

import 'SignUpPage.dart';

class LoginPage extends StatefulWidget {
  _LoginPageState createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logo_maximize.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            RaisedButton(
              child: Text(
                "Sign In",
              ),
              onPressed: onSignIn,
            ),
            RaisedButton(
              child: Text(
                "Sign Up",
              ),
              onPressed: onSignUp,
            ),
          ],
        ),
      ),
    );
  }

  void onSignIn() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignInPage()),
    );
  }

  void onSignUp() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignUpPage()),
    );
  }
}