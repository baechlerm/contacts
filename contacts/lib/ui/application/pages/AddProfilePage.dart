import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/widgets/PersonalProfileForm.dart';
import 'package:u_contact/ui/application/widgets/ProProfileForm.dart';

class AddProfilePage extends StatefulWidget {
  AddProfilePage({this.contacts});
  final List<Contact> contacts;

  final List<Tab> profileTabs = <Tab>[
    Tab(text: 'Personal'),
    Tab(text: 'Company'),
  ];

  _AddProfilePageState createState() {
    return _AddProfilePageState();
  }
}

class _AddProfilePageState extends State<AddProfilePage> with SingleTickerProviderStateMixin {
  Profile _newProfile = Profile();
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("New profile"),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(40),
          child: TabBar(
            controller: _tabController,
            tabs: widget.profileTabs,
          ),
        ),
      ),

      body: TabBarView(
        controller: _tabController,
        children: [
          SingleChildScrollView(
            child: PersonalProfileForm(
              profile: _newProfile,
              contacts: List<Contact>(),
            ),
          ),

          SingleChildScrollView(
            child: ProProfileForm(
              profile: _newProfile,
              contacts: widget.contacts,
            ),
          ),
        ],
      ),
    );
  }
}