import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/widgets/LocalContactForm.dart';

class AddLocalContactPage extends StatefulWidget {
  _AddLocalContactPageState createState() {
    return _AddLocalContactPageState();
  }
}

class _AddLocalContactPageState extends State<AddLocalContactPage> {
  Contact newContact = Contact();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("New contact"),
        ),
      ),
      body: SingleChildScrollView(
        child: LocalContactForm(contact: newContact,),
      ),
    );
  }
}