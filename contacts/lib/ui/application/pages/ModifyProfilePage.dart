import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/widgets/PersonalProfileForm.dart';
import 'package:u_contact/ui/application/widgets/ProProfileForm.dart';


class ModifyProfilePage extends StatefulWidget {
  ModifyProfilePage({this.editProfile});
  final Profile editProfile;

  _ModifyProfilePageState createState() {
    return _ModifyProfilePageState();
  }
}

class _ModifyProfilePageState extends State<ModifyProfilePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("uContact"),
        ),
      ),
      body: SingleChildScrollView(
        child: _chooseForm(),
      ),
    );
  }

  Widget _chooseForm() {
    if (widget.editProfile.name != null && widget.editProfile.name != "") {
      return PersonalProfileForm(profile: widget.editProfile, contacts: List<Contact>(),);
    }
    return ProProfileForm(profile: widget.editProfile, contacts: BlocProvider.of<AccountBloc>(context).bloc.allContacts,);
  }
}