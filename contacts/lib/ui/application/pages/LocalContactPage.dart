import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/widgets/LocalContactWidget.dart';

import '../MenuWidget.dart';

class LocalContactPage extends StatelessWidget {
  LocalContactPage({this.contact});

  final Contact contact;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("New contact"),
        ),
        actions: <Widget>[
          MenuWidget(),
        ],
      ),
      body: SingleChildScrollView(
        child: LocalContactWidget(contact: contact,),
      ),
    );
  }
}