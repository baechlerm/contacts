import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/widgets/LocalContactForm.dart';

class ModifyLocalContactPage extends StatefulWidget {
  ModifyLocalContactPage({this.contact});
  final Contact contact;

  _ModifyLocalContactPageState createState() {
    return _ModifyLocalContactPageState();
  }
}

class _ModifyLocalContactPageState extends State<ModifyLocalContactPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("New contact"),
        ),
      ),
      body: SingleChildScrollView(
        child: LocalContactForm(contact: widget.contact,),
      ),
    );
  }
}