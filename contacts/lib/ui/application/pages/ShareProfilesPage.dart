import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/pages/AddProfilePage.dart';
import 'package:u_contact/ui/application/widgets/ProfileWidget.dart';

import '../MenuWidget.dart';

class ShareProfilesPage extends StatefulWidget {
  static int indexTab = 0;

  _ShareProfilesPageState createState() {
    return _ShareProfilesPageState();
  }
}

class _ShareProfilesPageState extends State<ShareProfilesPage> with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    ShareProfilesPage.indexTab = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Profile>> (
      initialData: BlocProvider.of<AccountBloc>(context).bloc.userProfiles,
      stream: BlocProvider.of<AccountBloc>(context).bloc.profilesStream,
      builder: (context, snapshot) {
        final profiles = snapshot.data;
        List<Tab> profileTabs = List<Tab>();

        if (profiles == null || profiles.isEmpty) {
          return _noProfiles();
        }

        for (Profile profile in profiles) {
          profileTabs.add(Tab(text: profile.title));
        }
        _tabController = TabController(vsync: this, length: profileTabs.length);
        _tabController.animateTo(ShareProfilesPage.indexTab);
        _tabController.addListener(_handleTabSelection);

        return Scaffold(
          appBar: _createAppBar(profileTabs),

          body: _createTabBar(profiles),

          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: _onPressedAddProfile,
          ),
        );
      },
    );
  }

  void _handleTabSelection() {
    ShareProfilesPage.indexTab = _tabController.index;
  }

  void _onPressedAddProfile() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddProfilePage(contacts: BlocProvider.of<AccountBloc>(context).bloc.allContacts)),
    );
  }

  Widget _createAppBar(List<Tab> profileTabs) {
    return AppBar(
      centerTitle: false,
      title: FittedBox(
        child: Text("Profiles"),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: Align(
          alignment: Alignment.centerLeft,
          child: TabBar(
            controller: _tabController,
            isScrollable: true,
            tabs: profileTabs,
          ),
        ),
      ),
      actions: <Widget>[
        MenuWidget(),
      ],
    );
  }

  Widget _createTabBar(List<Profile> profiles) {
    List<Widget> tabs = List<Widget>();

    for (Profile profile in profiles) {
      tabs.add(_showTabBar(profile));
    }

    return TabBarView(
      controller: _tabController,
      children: tabs,
    );
  }

  Widget _showTabBar(profile) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: ProfileWidget(profile: profile,),
      ),
    );
  }

  Widget _noProfiles() {
    ShareProfilesPage.indexTab = -1;
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("Profiles"),
        ),
        actions: <Widget>[
          MenuWidget(),
        ],
      ),
      body: Center(
        child: Text("You don't have any profile. You should add one."),
      ),


      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _onPressedAddProfile,
      ),
    );
  }
}