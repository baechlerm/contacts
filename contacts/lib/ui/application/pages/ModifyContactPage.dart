import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/widgets/ContactForm.dart';

class ModifyContactPage extends StatefulWidget {
  ModifyContactPage({this.contact});
  final Contact contact;

  _ModifyContactPageState createState() {
    return _ModifyContactPageState();
  }
}

class _ModifyContactPageState extends State<ModifyContactPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: FittedBox(
          child: Text("New contact"),
        ),
      ),
      body: SingleChildScrollView(
        child: ContactForm(contact: widget.contact,),
      ),
    );
  }
}