import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/ContactLocal.dart';
import 'package:u_contact/data/IDatabase.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/MenuWidget.dart';
import 'package:u_contact/ui/application/pages/AddLocalContactPage.dart';
import 'package:u_contact/ui/application/pages/ModifyContactPage.dart';
import 'package:u_contact/ui/application/pages/ShareProfilesPage.dart';
import 'package:u_contact/ui/application/widgets/ContactsListWidget.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  TabController _tabController;
  bool _isInitialized = false;
  AccountBloc accountBloc;

  // Search
  bool isSearching = false;
  String searchText = "";
  final TextEditingController searchController = new TextEditingController();

  // Animation
  bool isOpened = false;

  @override
  void initState() {
    searchController.addListener(() {
      setState(() {
        searchText = searchController.text;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    accountBloc = BlocProvider.of<AccountBloc>(context).bloc;

    if (!_isInitialized) {
      getData();
    }

    return StreamBuilder<List<Contact>> (
      initialData: accountBloc.allContacts,
      stream: accountBloc.contactsStream,
      builder: (context, snapshot) {
        final contacts = List<Contact>();

        snapshot.data.forEach((cont) {
          if (cont.profileId != null && cont.profileId != "") {
            contacts.add(cont);
          } else if (cont.name != null && cont.name != "") {
            contacts.add(cont);
          }
        });
        List<Tab> categoriesTab = List<Tab>();

        if (contacts == null || contacts.isEmpty) {
          return _noContacts();
        }

        categoriesTab.add(Tab(text: "All contacts",));
        if (accountBloc.connectedUser.categories != null && accountBloc.connectedUser.categories.isNotEmpty) {
          for (String categorie in accountBloc.connectedUser.categories) {
            categoriesTab.add(Tab(text: categorie));
          }
        }
        _tabController = TabController(vsync: this, length: categoriesTab.length);

        return Scaffold(
          appBar: _createAppBar(categoriesTab),

          body: _createTabBar(contacts),

          floatingActionButton: addFloatingButton(),
        );
      },
    );
  }

  Widget _createAppBar(List<Tab> categoryTabs) {
    if (isSearching) {
      return AppBar(
        backgroundColor: Colors.white,
        centerTitle: false,
        title: TextField(
          controller: searchController,
          maxLines: 1,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            color: Color.fromRGBO(0x2e, 0x7d, 0x32, 1),
            onPressed: () {
              setState(() {
                isSearching = false;
                searchText = "";
              });
              searchController.text = "";
            },
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: Container(
            color: Color.fromRGBO(0x2e, 0x7d, 0x32, 1),
          child: Align(
            alignment: Alignment.centerLeft,
              child: TabBar(
                controller: _tabController,
                isScrollable: true,
                tabs: categoryTabs,
              ),
            ),
          ),
        ),
      );
    }

    return AppBar(
      leading: IconButton(
        icon: Icon(
          Icons.person,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ShareProfilesPage()),
          );
        },
      ),
      centerTitle: false,
      title: FittedBox(
        child: Text("uContact"),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () {
            setState(() {
              isSearching = true;
            });
          },
        ),
        MenuWidget(),
      ],
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: Align(
          alignment: Alignment.centerLeft,
          child: TabBar(
            controller: _tabController,
            isScrollable: true,
            tabs: categoryTabs,
          ),
        ),
      ),
    );
  }

  String getDisplayName(Contact contact) {
    if (contact.profileId == null || contact.profileId == "") {
      return contact.name + " " + contact.firstname;
    }

    if (contact.profile.name != null && contact.profile.name != "") {
      return contact.profile.name + " " + contact.profile.firstname;
    }
    return contact.profile.companyName;
  }

  Widget _createTabBar(List<Contact> contacts) {
    List<Widget> tabs = List<Widget>();

    List<Contact> allContact = List<Contact>();
    if (isSearching) {
      contacts.forEach((contact) {
        if (getDisplayName(contact).contains(searchText)) {
          allContact.add(contact);
        }
      });
    } else {
      allContact.addAll(contacts);
    }

    tabs.add(_createContactsList(allContact));

    BlocProvider.of<AccountBloc>(context).bloc.connectedUser.categories.forEach((cat) {
      List<Contact> catContacts = List<Contact>();
      contacts.forEach((cont) {
        if (cont.profileId != null && cont.profileId != "" && cont.categories.contains(cat)) {
          if (isSearching) {
            if (getDisplayName(cont).contains(searchText)) {
              catContacts.add(cont);
            }
          } else {
            catContacts.add(cont);
          }
        }
      });
      tabs.add(_createContactsList(catContacts));
    });

    return TabBarView(
      controller: _tabController,
      children: tabs,
    );
  }

  Widget _createContactsList(List<Contact> contacts) {
    return Card(
      child: ContactsListWidget(contacts: contacts,),
    );
  }

  Widget _noContacts() {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.person,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ShareProfilesPage()),
            );
          },
        ),
        centerTitle: false,
        title: FittedBox(
          child: Text("uContact"),
        ),
        actions: <Widget>[
          MenuWidget(),
        ],
      ),
      body: Center(
        child: Text("You don't have any contact. You should add one."),
      ),
      floatingActionButton: addFloatingButton(),
    );
  }

  void getData() async {
    _isInitialized = true;

    if (await Permission.contacts.request().isGranted) {
      ContactLocal().getLocalContacts().then((results) {
        accountBloc.localContactsRetrieve(results);
      });
    }

    DatabaseProvider.of(context).data.getContacts(accountBloc.connectedUser.contacts).then((results) {
      accountBloc.contactsRetrieve(results);
    });

    DatabaseProvider.of(context).data.getProfilesFromUser(accountBloc.connectedUser).then((results) {
      accountBloc.profilesRetrieve(results);
    });
  }

  Widget addFloatingButton() {
    if (isOpened) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "Create contact",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                width: 10,
              ),
              FloatingActionButton(
                onPressed: _onPressedCreateContact,
                child: Icon(
                  Icons.contacts,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child:
              IntrinsicHeight(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Scan contact",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      width: 10,
                    ),
                    FloatingActionButton(
                      onPressed: _onPressedAddContact,
                      child: Icon(
                        Icons.image,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              )
          ),
          FloatingActionButton(
            onPressed: () {
              setState(() {
                isOpened = !isOpened;
              });
            },
            splashColor: Color.fromRGBO(0xe5, 0x53, 0x53, 1),
            backgroundColor: Color.fromRGBO(0xe5, 0x73, 0x73, 1),
            child: Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        ],
      );
    }

    return FloatingActionButton(
      onPressed: () {
        setState(() {
          isOpened = !isOpened;
        });
      },
      child: Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  void _onPressedAddContact() async {
    setState(() {
      isOpened = !isOpened;
    });
    if (await Permission.camera.request().isGranted) {
      var result = await BarcodeScanner.scan();

      if (result.type == ResultType.Barcode) {
        IDatabase database = DatabaseProvider.of(context).data;
        database.profileDoesExist(result.rawContent).then((doesExist) async {
          if (doesExist) {
            Profile profile = await database.getProfile(result.rawContent);
            Contact contact = Contact.fromProfile(profileId: profile.id, profile: profile);

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ModifyContactPage(contact: contact)),
            );
          }
        });
      }
    }
  }
  void _onPressedCreateContact() {
    setState(() {
      isOpened = !isOpened;
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddLocalContactPage()),
    );
  }
}