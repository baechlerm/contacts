import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/IDatabase.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/pages/ModifyContactPage.dart';
import 'package:u_contact/ui/application/pages/ShareProfilesPage.dart';

class MenuWidget extends StatefulWidget {
  _MenuWidgetState createState() {
    return _MenuWidgetState();
  }
}

class _MenuWidgetState extends State<MenuWidget> {
  void _select(MenuItemChoice choice) {
    Navigator.popUntil(context, (_) => !Navigator.canPop(context));
    switch (choice) {
      case MenuItemChoice.SHARE:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ShareProfilesPage()),
        );
        break;
      case MenuItemChoice.ADD:
        _onPressedAddContact();
        break;
      case MenuItemChoice.SIGNOUT:
        DatabaseProvider.of(context).data.signOut();
        BlocProvider.of<AccountBloc>(context).bloc.userRetrieve(null);
        break;
    }
  }

  Widget _showMenuIcon() {
    return PopupMenuButton<MenuItemChoice>(
      onSelected: _select,
      itemBuilder: (BuildContext context) {
        return [
          PopupMenuItem<MenuItemChoice> (
            value: MenuItemChoice.SHARE,
            child: Text("Share profiles"),
          ),
          PopupMenuItem<MenuItemChoice> (
            value: MenuItemChoice.ADD,
            child: Text("New contact"),
          ),
          PopupMenuItem<MenuItemChoice> (
            value: MenuItemChoice.SIGNOUT,
            child: Text("Sign out"),
          ),
        ];
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _showMenuIcon();
  }

  void _onPressedAddContact() async {
    if (await Permission.camera.request().isGranted) {
      var result = await BarcodeScanner.scan();

      if (result.type == ResultType.Barcode) {
        IDatabase database = DatabaseProvider.of(context).data;
        database.profileDoesExist(result.rawContent).then((doesExist) async {
          if (doesExist) {
            Profile profile = await database.getProfile(result.rawContent);
            Contact contact = Contact.fromProfile(profileId: profile.id, profile: profile);

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ModifyContactPage(contact: contact)),
            );
          }
        });
      }
    }
  }
}

enum MenuItemChoice {
  SHARE,
  ADD,
  SIGNOUT
}