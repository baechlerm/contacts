import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';

import '../../../main.dart';

class PersonContactForm extends StatefulWidget {
  PersonContactForm({this.contacts, this.profileIds});

  final List<Contact> contacts;
  final List<String> profileIds;

  @override
  _PersonContactFormState createState() {
    return _PersonContactFormState();
  }
}

class _PersonContactFormState extends State<PersonContactForm> {
  final int numberOfSuggest = 3;
  List<String> displayContactSelected = List<String>();
  List<String> profileContactSelected = List<String>();

  TextEditingController _personContactController = TextEditingController();
  List<Contact> contacts = List<Contact>();
  List<String> contactsToShow = List<String>();
  List<String> profileIdShow = List<String>();
  String searchText = "";

  @override
  void initState() {
    widget.contacts.forEach((contact) {
      if (contact.profile != null) {
        for (int i = 0; i < widget.profileIds.length; i++) {
          if (widget.profileIds[i] == contact.profileId) {
            displayContactSelected.add(getContactName(contact));
            profileContactSelected.add(contact.profileId);
            break;
          }
        }

        contacts.add(contact);
      }
    });

    _personContactController.addListener(() {
      setState(() {
        searchText = _personContactController.text;
      });
    });

    super.initState();
  }

  String getContactName(Contact contact) {
    if (contact.profile.name != null && contact.profile.name != "") {
      return contact.profile.name + " " + contact.profile.firstname;
    }
    return contact.profile.companyName;
  }

  Widget getListTile() {
    if (searchText.isEmpty) {
      return Container();
    }

    contactsToShow = List<String>();
    profileIdShow = List<String>();

    int index = 0;
    for (int i = 0; i < contacts.length; i++) {
      String displayName = getContactName(contacts[i]);
      if (displayName.contains(searchText)) {
        contactsToShow.add(displayName);
        profileIdShow.add(contacts[i].profileId);
        index++;
      }
      if (index >= numberOfSuggest) {
        break;
      }
    }
    if (index == 0) {
      return Container();
    }

    List<Widget> listContacts = List<Widget>();
    for (int i = 0; i < contactsToShow.length; i++) {
      listContacts.add(
        ListTile(
          contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          leading: CircleAvatar(
            child: Icon(Icons.person),
          ),
          title: Text(contactsToShow[i]),
          onTap: () {
            if (!profileContactSelected.contains(profileIdShow[i])) {
              setState(() {
                displayContactSelected.add(contactsToShow[i]);
                profileContactSelected.add(profileIdShow[i]);
                widget.profileIds.add(profileIdShow[i]);
              });
            }
          },
        ),
      );
    }

    return Column(
      children: listContacts,
    );
  }

  Widget getButtonContacts() {
    if (displayContactSelected.length <= 0) {
      return Container();
    }

    List<Widget> chips = List<Widget>();
    for (int i = 0; i < displayContactSelected.length; i++) {
      chips.add(
        Chip(
          label: Text(displayContactSelected[i]),
          deleteIcon: Icon(Icons.close),
          onDeleted: () {
            widget.profileIds.removeWhere((data) {
              return data == profileContactSelected[i];
            });

            setState(() {
              displayContactSelected.removeAt(i);
              profileContactSelected.removeAt(i);
            });
          },
        ),
      );
    }

    return Wrap(
      direction: Axis.horizontal,
      children: chips,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        getButtonContacts(),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            controller: _personContactController,
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Person of contact"),
          ),
        ),
        getListTile(),
      ],
    );
  }
}