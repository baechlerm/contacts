import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/pages/ContactPage.dart';
import 'package:u_contact/ui/application/pages/ModifyContactPage.dart';

class PersonContactWidget extends StatefulWidget {
  PersonContactWidget({this.profileIds});
  final List<String> profileIds;

  @override
  _PersonContactWidgetState createState() {
    return _PersonContactWidgetState();
  }
}

class _PersonContactWidgetState extends State<PersonContactWidget> {
  List<Profile> profiles = List<Profile>();
  List<Contact> contacts = List<Contact>();
  bool isInit = false;

  @override
  Widget build(BuildContext context) {
    if (widget.profileIds == null || widget.profileIds.length <= 0) {
      return Container();
    }

    if (!isInit) {
      isInit = true;

      for (String profileId in widget.profileIds) {
        DatabaseProvider.of(context).data.getProfile(profileId).then((profile) {
          String name = "";
          if (profile.name != null && profile.name != "") {
            name = profile.name + " " + profile.firstname;
          } else {
            name = profile.companyName;
          }

          setState(() {
            profiles.add(profile);
          });
        });
      }

      BlocProvider.of<AccountBloc>(context).bloc.allContacts.forEach((contact) {
        contacts.add(contact);
      });
    }

    if (profiles == null || profiles.length <= 0) {
      return Container();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _showPersonContact(),
        Wrap(
          direction: Axis.horizontal,
          children: _getChips(),
        ),
      ],
    );
  }

  Widget _showPersonContact() {
    return Row(
      children: <Widget>[
        Text(
          "Person of contact",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 17,
          ),
        ),
      ],
    );
  }

  String _getDisplayName(Profile profile) {
    if (profile.name == null || profile.name == "") {
      return profile.companyName;
    }
    return profile.name + " " + profile.firstname;
  }

  List<Widget> _getChips() {
    List<Widget> chips = List<Widget>();

    for (var profile in profiles) {
      chips.add(
        ActionChip(
          label: Text(_getDisplayName(profile)),
          onPressed: () {
            Contact myContact;
            for (int i = 0; i < contacts.length; i++) {
              if (contacts[i].profileId == profile.id) {
                myContact = contacts[i];
                break;
              }
            }
            if (myContact == null) {
              myContact = Contact.fromProfile(profileId: profile.id, profile: profile);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ModifyContactPage(contact: myContact,)),
              );
            } else {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ContactPage(contact: myContact,)),
              );
            }
          },
        )
      );
    }

    return chips;
  }
}