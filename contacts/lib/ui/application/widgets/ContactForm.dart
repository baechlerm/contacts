import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/main.dart';
import 'package:u_contact/ui/application/share/PersonContactWidget.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactForm extends StatefulWidget {
  ContactForm({this.contact});
  final Contact contact;

  _ContactFormState createState() {
    return _ContactFormState();
  }
}

class _ContactFormState extends State<ContactForm> {
  final formKey = GlobalKey<FormState>();

  // Data
  String errorMessage = "";
  bool isNewContact = true;

  final TextEditingController _descController = TextEditingController();
  List<String> _categories = List<String>();
  List<String> _categoriesContactList = List<String>();
  List<TextEditingController> _categoriesValueControllers = List<TextEditingController>();

  @override
  void initState() {
    super.initState();

    _descController.text = widget.contact.description;

    if (widget.contact.categories == null || widget.contact.categories.isEmpty) {
      _categoriesContactList.add('New category');
      _categoriesValueControllers.add(TextEditingController());
    } else {
      widget.contact.categories.forEach((value) {
        _categoriesContactList.add(value);
        _categoriesValueControllers.add(TextEditingController());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    AccountBloc accountBloc = BlocProvider.of<AccountBloc>(context).bloc;

    for (int i = 0; i < accountBloc.allContacts.length; i++) {
      if (accountBloc.allContacts[i].profileId == widget.contact.profileId) {
        isNewContact = false;
        break;
      }
    }
    _categories = List<String>();
    _categories.addAll(accountBloc.connectedUser.categories);
    _categories.insert(0, 'New category');


    return Form(
        child: Column(
          children: <Widget>[
            Card(
              child: Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Column(
                  children: <Widget>[
                    _showName(),
                    _showPhone(),
                    _showMail(),
                    _showAdress(),
                    _showDesciption(),
                    _showSocial(),
                    _showCompany(),
                    _showJobTitle(),

                    _showProPhone(),
                    _showProMail(),
                    PersonContactWidget(profileIds: widget.contact.profile.personsContact,),
                    _showWebsite(),
                    _showProAdress(),
                    _showProDesciption(),
                    _showProSocial(),

                    _showCustoms(),
                  ],
                ),
              ),
            ),
            Card(
              child: _showForm(),
            ),
            _showErrorMessage(),
            _showActions(),
            //_showActions();
          ],
        )
    );
  }

  Widget _showName() {
    if (widget.contact.profile.name == null || widget.contact.profile.name == "") {
      return Container();
    }
    String abr = (widget.contact.profile.name.substring(0,1) + widget.contact.profile.firstname.substring(0,1)).toUpperCase();
    String displayName = widget.contact.profile.name + " " + widget.contact.profile.firstname;

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showPhone() {
    if (widget.contact.profile.phoneNumber != null && widget.contact.profile.phoneNumber != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                widget.contact.profile.phoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + widget.contact.profile.phoneNumber);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + widget.contact.profile.phoneNumber);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showMail() {
    if (widget.contact.profile.mail != null && widget.contact.profile.mail != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                widget.contact.profile.mail,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + widget.contact.profile.mail);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  void _launchURL(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        print("Error url");
        print(url);
      }
    } catch (e) {
      print("Error with the emulator.");
    }
  }

  Widget _showAdress() {
    if (widget.contact.profile.npa == null || widget.contact.profile.npa == "" || widget.contact.profile.city == null || widget.contact.profile.city == "") {
      return Container();
    }

    Widget street;
    if (widget.contact.profile.street != null && widget.contact.profile.street != "")  {
      street = Text(
        widget.contact.profile.street,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            widget.contact.profile.npa + " " + widget.contact.profile.city,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showDesciption() {
    if (widget.contact.profile.personalDescription != null && widget.contact.profile.personalDescription != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    widget.contact.profile.personalDescription,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showCompany() {
    if (widget.contact.profile.companyName == null || widget.contact.profile.companyName == "") {
      return Container();
    }

    String abr = widget.contact.profile.companyName.substring(0,1);
    String displayName = widget.contact.profile.companyName;

    double littleCompany = 0;
    if (widget.contact.profile.name != null && widget.contact.profile.name != "") {
      littleCompany = 5;
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0 + littleCompany, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30 - littleCompany,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20 - littleCompany,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20 - littleCompany,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showJobTitle() {
    if (widget.contact.profile.jobTitle != null && widget.contact.profile.jobTitle != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Work",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    widget.contact.profile.jobTitle,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProPhone() {
    if (widget.contact.profile.jobPhoneNumber != null && widget.contact.profile.jobPhoneNumber != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                widget.contact.profile.jobPhoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + widget.contact.profile.jobPhoneNumber);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + widget.contact.profile.jobPhoneNumber);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProMail() {
    if (widget.contact.profile.jobMail != null && widget.contact.profile.jobMail != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                widget.contact.profile.jobMail,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + widget.contact.profile.jobMail);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showWebsite() {
    if (widget.contact.profile.website != null && widget.contact.profile.website.length > 0) {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                widget.contact.profile.website[0],
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.web,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL(widget.contact.profile.website[0]);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProAdress() {
    if (widget.contact.profile.companyNpa == null || widget.contact.profile.companyNpa == "" || widget.contact.profile.companyCity == null || widget.contact.profile.companyCity == "") {
      return Container();
    }

    Widget street;
    if (widget.contact.profile.companyStreet != null && widget.contact.profile.companyStreet != "")  {
      street = Text(
        widget.contact.profile.companyStreet,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            widget.contact.profile.companyNpa + " " + widget.contact.profile.companyCity,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showProDesciption() {
    if (widget.contact.profile.companyDescription != null && widget.contact.profile.companyDescription != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Company Description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    widget.contact.profile.companyDescription,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showSocial() {
    if (widget.contact.profile.name == null || widget.contact.profile.name == "") {
      return Container();
    }

    return _socialWidget();
  }

  Widget _showProSocial() {
    if (widget.contact.profile.name == null || widget.contact.profile.name == "") {
      return _socialWidget();
    }

    return Container();
  }

  Widget _socialWidget() {
    List<Widget> socials = List<Widget>();

    if (widget.contact.profile.facebook != null && widget.contact.profile.facebook != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.facebook),
            onPressed: () {},
          )
      );
    }

    if (widget.contact.profile.twitter != null && widget.contact.profile.twitter != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.twitter),
            onPressed: () {},
          )
      );
    }

    if (widget.contact.profile.snapchat != null && widget.contact.profile.snapchat != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.snapchat),
            onPressed: () {},
          )
      );
    }

    if (widget.contact.profile.instagram != null && widget.contact.profile.instagram != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.instagram),
            onPressed: () {},
          )
      );
    }

    if (widget.contact.profile.youtube != null && widget.contact.profile.youtube != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.youtube),
            onPressed: () {},
          )
      );
    }

    if (widget.contact.profile.linkedin != null && widget.contact.profile.linkedin != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.linkedin),
            onPressed: () {},
          )
      );
    }

    if (socials.length <= 0) {
      return Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: socials,
      ),
    );
  }

  Widget _showCustoms() {
    if (widget.contact.profile.customs == null || widget.contact.profile.customs.length <= 0) {
      return Container();
    }

    List<Widget> customs = List<Widget>();
    customs.add(
      Row(
        children: <Widget>[
          Text(
            "Other informations",
            style: TextStyle(
              decoration: TextDecoration.underline,
              fontSize: 17,
            ),
          ),
        ],
      ),
    );

    widget.contact.profile.customs.forEach((key, value) {
      customs.add(
        Row(
          children: <Widget>[
            Text(
              key.substring(key.indexOf("_") + 1) + ": " + value,
            ),
          ],
        ),
      );
    });

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        children: customs,
      ),
    );
  }

  Widget _showErrorMessage() {
    if (errorMessage == "") {
      return Container();
    }

    return Text(
      errorMessage,
      style: TextStyle(
        color: Colors.red,
      ),
    );
  }

  Widget _showForm() {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Contact information",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
              child: TextFormField(
                controller: _descController,
                maxLines: 5,
                keyboardType: TextInputType.text,
                autofocus: false,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),

                  filled: true,
                  fillColor: Color.fromRGBO(0xE8, 0xF5, 0xE9, 1),

                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    borderSide: BorderSide(color: Color(00000000)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    borderSide: BorderSide(color: Color(00000000)),
                  ),
                  hintText: "Contact description",
                ),
              ),
            ),
            _createCategories(),
          ],
        ),
      ),
    );
  }

  Widget _createCategories() {
    List<Widget> categories = List<Widget>();

    if (_categoriesContactList.length <= 0) {
      return Container();
    }

    for (int i = 0; i < _categoriesContactList.length; i++) {
      if (_categoriesContactList[i] == 'New category') {
        categories.add(
          Row(
            children: <Widget>[
              DropdownButton<String>(
                  value: _categoriesContactList[i],
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 12,
                  onChanged: (String newValue) {
                    setState(() {
                      _categoriesContactList[i] = newValue;
                    });
                  },
                  items: _categories
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  })
                      .toList(),
                ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                  child: TextFormField(
                    controller: _categoriesValueControllers[i],
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autofocus: false,
                    decoration: MyApp.getInputDecoration("Value"),
                  ),
                ),
              ),
              FlatButton(
                child: Icon(
                  Icons.delete,
                ),
                onPressed: () {
                  setState(() {
                    _categoriesContactList.removeAt(i);
                  });
                },
              )
            ],
          ),
        );
      } else {
        categories.add(
          Row(
            children: <Widget>[
              Expanded(
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: _categoriesContactList[i],
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 12,
                  onChanged: (String newValue) {
                    setState(() {
                      _categoriesContactList[i] = newValue;
                    });
                  },
                  items: _categories
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  })
                      .toList(),
                ),
              ),
              FlatButton(
                child: Icon(
                  Icons.delete,
                ),
                onPressed: () {
                  setState(() {
                    _categoriesContactList.removeAt(i);
                  });
                },
              )
            ],
          ),
        );
      }
    }

    categories.add(
      RaisedButton(
        child: Text("+"),
        onPressed: () {
          setState(() {
            _categoriesContactList.add('New category');
            _categoriesValueControllers.add(TextEditingController());
          });
        },
      ),
    );


    return Column(
      children: categories,
    );
  }

  Widget _showActions() {
    Widget deleteButton;
    Widget spaceBetweenContainer;

    if (isNewContact) {
      deleteButton = Container();
      spaceBetweenContainer = Container();
    } else {
      spaceBetweenContainer = Container(
        width: 10,
      );
      deleteButton = RaisedButton(
        child: Text("Delete"),
        onPressed: _onDelete,
        color: Color.fromRGBO(0xe5, 0x73, 0x73, 1),
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        deleteButton,
        spaceBetweenContainer,
        RaisedButton(
          child: Text("Save"),
          onPressed: _onSave,
        ),
      ],
    );
  }

  void _onDelete() {
    DatabaseProvider.of(context).data.deleteContactFromUser(widget.contact,BlocProvider.of<AccountBloc>(context).bloc.connectedUser).then((contact) {
      BlocProvider.of<AccountBloc>(context).bloc.deleteContact(widget.contact);
    });
    Navigator.pop(context);
    Navigator.pop(context);
  }

  void _onSave() {
    if (_saveContact()) {
      try {
        AccountBloc accountBloc = BlocProvider.of<AccountBloc>(context).bloc;

        if (isNewContact) {
          DatabaseProvider.of(context).data.newContactFromUser(widget.contact, accountBloc.connectedUser);
          accountBloc.contactsRetrieve(accountBloc.connectedUser.contacts);
        } else {
          accountBloc.updateContact(widget.contact);
          DatabaseProvider.of(context).data.updateUser(accountBloc.connectedUser);
        }
        Navigator.pop(context);
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  bool _saveContact() {
    widget.contact.description = _descController.text;
    widget.contact.categories = List<String>();

    for (int i = 0; i < _categoriesContactList.length; i++) {
      if (_categoriesContactList[i] == 'New category') {
        if (_categoriesValueControllers[i].text != 'New category' && !widget.contact.categories.contains(_categoriesValueControllers[i].text) && _categoriesValueControllers[i].text != "") {
          widget.contact.categories.add(_categoriesValueControllers[i].text);

          if (!BlocProvider.of<AccountBloc>(context).bloc.connectedUser.categories.contains(_categoriesValueControllers[i].text)) {
            BlocProvider.of<AccountBloc>(context).bloc.connectedUser.categories.add(_categoriesValueControllers[i].text);
          }
        }
      } else if (!widget.contact.categories.contains(_categoriesContactList[i])) {
        widget.contact.categories.add(_categoriesContactList[i]);
      }
    }
    return true;
  }
}