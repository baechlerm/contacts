import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/main.dart';
import 'package:u_contact/ui/application/pages/ShareProfilesPage.dart';
import 'package:u_contact/ui/application/share/PersonContactForm.dart';

class ProProfileForm extends StatefulWidget {
  ProProfileForm({this.profile, this.contacts});
  final Profile profile;
  final List<Contact> contacts;

  _ProProfileFormState createState() {
    return _ProProfileFormState();
  }
}

class _ProProfileFormState extends State<ProProfileForm> {
  // Data
  String errorMessage = "";
  bool isNewProfile = false;

  // Form Datas
  final TextEditingController _titleController = TextEditingController(text: "New profile");

  final TextEditingController _companyNameController = TextEditingController();
  final TextEditingController _jopPhoneController = TextEditingController();
  final TextEditingController _jobMailController = TextEditingController();
  final TextEditingController _companyWebsiteController = TextEditingController();
  final TextEditingController _npaController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  final TextEditingController _facebookController = TextEditingController();
  final TextEditingController _twitterController = TextEditingController();
  final TextEditingController _linkedinController = TextEditingController();

  List<TextEditingController> _customTagControllers = List<TextEditingController>();
  List<TextEditingController> _customValueControllers = List<TextEditingController>();
  List<String> profileIds = List<String>();

  @override
  void initState() {
    super.initState();
    if (widget.profile.name == null || widget.profile.name == "") {
      isNewProfile = true;
    }

    _titleController.text = widget.profile.title;

    _companyNameController.text = widget.profile.companyName;
    _jopPhoneController.text = widget.profile.jobPhoneNumber;
    _jobMailController.text = widget.profile.jobMail;
    _companyWebsiteController.text = widget.profile.website.isEmpty ? "" : widget.profile.website[0];
    _npaController.text = widget.profile.companyNpa;
    _cityController.text = widget.profile.companyCity;
    _streetController.text = widget.profile.companyStreet;
    _descriptionController.text = widget.profile.companyDescription;

    _facebookController.text = widget.profile.facebook;
    _twitterController.text = widget.profile.twitter;
    _linkedinController.text = widget.profile.linkedin;

    profileIds = widget.profile.personsContact;

    if (widget.profile.customs == null || widget.profile.customs.isEmpty) {
      _customTagControllers.add(TextEditingController());
      _customValueControllers.add(TextEditingController());
    } else {
      widget.profile.customs.forEach((key, value) {
        if (key.contains("_")) {
          key = key.substring(key.indexOf("_") + 1);
        }

        _customTagControllers.add(TextEditingController(text: key));
        _customValueControllers.add(TextEditingController(text: value));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: <Widget>[
          _showTitle(),
          _showCompanyInfoCard(),
          _showPersonOfContact(),
          _showSocialCard(),
          _showCustomsCard(),
          _showErrorMessage(),
          _showActions(),
        ],
      ),
    );
  }

  Widget _showPersonOfContact() {
    return Card(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ExpansionTile(
            initiallyExpanded: false,
            title: Text("Persons of contact"),
            children: <Widget>[
              PersonContactForm(contacts: widget.contacts, profileIds: profileIds,),
            ],
          ),
        ),
      ),
    );
  }

  Widget _showTitle() {
    return Card(
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: TextFormField(
          controller: _titleController,
          maxLines: 1,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(0),
            border: InputBorder.none,
            hintText: 'Profile title',
          ),
        ),
      ),
    );
  }

  Widget _showErrorMessage() {
    if (errorMessage == "") {
      return Container();
    }

    return Text(
      errorMessage,
      style: TextStyle(
        color: Colors.red,
      ),
    );
  }

  Widget _showCompanyInfoCard() {
    return Card(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ExpansionTile(
            initiallyExpanded: true,
            title: Text("Company data"),
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _companyNameController,
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Name"),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _jopPhoneController,
                  maxLines: 1,
                  keyboardType: TextInputType.phone,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Phone number"),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  maxLines: 1,
                  controller: _jobMailController,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Mail"),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  maxLines: 1,
                  controller: _companyWebsiteController,
                  keyboardType: TextInputType.url,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Website"),
                ),
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                      child: TextFormField(
                        controller: _npaController,
                        maxLines: 1,
                        keyboardType: TextInputType.number,
                        autofocus: false,
                        decoration: MyApp.getInputDecoration("Code"),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                      child: TextFormField(
                        controller: _cityController,
                        maxLines: 1,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        decoration: MyApp.getInputDecoration("City"),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _streetController,
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Street"),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _descriptionController,
                  maxLines: 5,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),

                    filled: true,
                    fillColor: Color.fromRGBO(0xE8, 0xF5, 0xE9, 1),

                    border: InputBorder.none,
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      borderSide: BorderSide(color: Color(00000000)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      borderSide: BorderSide(color: Color(00000000)),
                    ),
                    hintText: "Description",
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _showSocialCard() {
    return Card(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ExpansionTile(
            initiallyExpanded: false,
            title: Text("Social networks"),
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _facebookController,
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Facebook"),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _twitterController,
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Twitter"),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _linkedinController,
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Linkedin"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _showCustomsCard() {
    return Card(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: ExpansionTile(
            initiallyExpanded: false,
            title: Text("Others"),
            children: <Widget>[
              Column(
                children: _showCustoms(),
              ),
              Center(
                child: RaisedButton(
                  child: Text("+"),
                  onPressed: () {
                    setState(() {
                      _customTagControllers.add(TextEditingController());
                      _customValueControllers.add(TextEditingController());
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _showCustoms() {
    List<Widget> customs = List<Widget>();

    for (int i = 0; i < _customValueControllers.length; i++) {
      customs.add(
        Row(
          children: <Widget>[
            Flexible(
              child: Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _customTagControllers[i],
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Tag"),
                ),
              ),
            ),
            Flexible(
              child: Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  controller: _customValueControllers[i],
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Value"),
                ),
              ),
            ),
            FlatButton(
              child: Icon(
                Icons.delete,
                size: 20,
              ),
              onPressed: () {
                setState(() {
                  _customTagControllers.removeAt(i);
                  _customValueControllers.removeAt(i);
                });
              },
            )
          ],
        ),
      );
    }

    return customs;
  }

  Widget _showActions() {
    Widget deleteButton;
    Widget spaceBetweenContainer;

    if (isNewProfile) {
      deleteButton = Container();
      spaceBetweenContainer = Container();
    } else {
      spaceBetweenContainer = Container(
        width: 10,
      );
      deleteButton = RaisedButton(
        child: Text("Delete"),
        onPressed: _onDelete,
        color: Color.fromRGBO(0xe5, 0x73, 0x73, 1),
      );
    }


    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        deleteButton,
        spaceBetweenContainer,
        RaisedButton(
          child: Text("Save"),
          onPressed: _onSave,
        ),
      ],
    );
  }

  void _onDelete() {
    if (ShareProfilesPage.indexTab != 0) {
      ShareProfilesPage.indexTab--;
    }
    DatabaseProvider.of(context).data.deleteProfileFromUser(widget.profile, BlocProvider.of<AccountBloc>(context).bloc.connectedUser).then((value) {
      BlocProvider.of<AccountBloc>(context).bloc.deleteProfile(widget.profile);
    });
    Navigator.pop(context);
  }

  void _onSave() {
    if (_saveProfile()) {
      try {
        AccountBloc accountBloc = BlocProvider.of<AccountBloc>(context).bloc;

        if (isNewProfile) {
          accountBloc.addProfile(widget.profile);
          DatabaseProvider.of(context).data.newProfileFromUser(widget.profile, accountBloc.connectedUser).then((profile) {
            widget.profile.id = profile.id;
          });
        } else {
          accountBloc.updateProfile(widget.profile);
          DatabaseProvider.of(context).data.updateProfile(widget.profile);
        }
        Navigator.pop(context);
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  bool _saveProfile() {
    if (_titleController.text == "" || _companyNameController.text == "") {
      setState(() {
        errorMessage = "Title and the company name must be provided!";
      });

      return false;
    }

    for (int i = 0; i < _customValueControllers.length; i++) {
      if (_customValueControllers[i].text != "") {
        widget.profile.customs[i.toString() + "_" + _customTagControllers[i].text] = _customValueControllers[i].text;
      }
    }

    widget.profile.title = _titleController.text;

    widget.profile.companyName = _companyNameController.text;
    widget.profile.jobPhoneNumber = _jopPhoneController.text;
    widget.profile.jobMail = _jobMailController.text;

    widget.profile.companyNpa = _npaController.text;
    widget.profile.companyCity = _cityController.text;
    widget.profile.companyStreet = _streetController.text;
    widget.profile.companyDescription = _descriptionController.text;
    widget.profile.website = List<String>();
    if (_companyWebsiteController.text != "") {
      widget.profile.website.add(_companyWebsiteController.text);
    }

    widget.profile.facebook = _facebookController.text;
    widget.profile.twitter = _twitterController.text;
    widget.profile.linkedin = _linkedinController.text;
    widget.profile.personsContact = profileIds;
    return true;
  }
}