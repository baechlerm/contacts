import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/pages/LocalContactPage.dart';
import 'package:u_contact/ui/application/pages/ModifyLocalContactPage.dart';
import 'package:u_contact/ui/application/widgets/LocalContactForm.dart';
import 'package:url_launcher/url_launcher.dart';

class LocalContactWidget extends StatefulWidget {
  LocalContactWidget({this.contact});

  final Contact contact;

  @override
  _LocalContactWidgetState createState() {
    return _LocalContactWidgetState();
  }
}

class _LocalContactWidgetState extends State<LocalContactWidget> {
  Contact contact;

  @override
  void initState() {
    super.initState();
    contact = widget.contact;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              _showName(),
              _showPhone(),
              _showMail(),
              _showAdress(),
              _showWork(),
            ],
          ),
        )
    );
  }

  Widget _showName() {
    String abr = (contact.name.substring(0,1) + contact.firstname.substring(0,1)).toUpperCase();
    String displayName = contact.name + " " + contact.firstname;

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.edit),
            iconSize: 25,
            onPressed: (){
              _onEdit();
            },
          )
        ],
      ),
    );
  }

  Future<void> _onEdit() async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ModifyLocalContactPage(contact: contact,)), // TODO
    );
    setState(() {
    });
  }

  Widget _showPhone() {
    if (contact.phone != null && contact.phone != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.phone,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + contact.phone);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + contact.phone);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showMail() {
    if (contact.email != null && contact.email != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.email,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + contact.email);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  void _launchURL(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        print("Error url");
        print(url);
      }
    } catch (e) {
      print("Error with the emulator.");
    }
  }

  Widget _showAdress() {
    if (contact.npa == null || contact.npa == "" || contact.city == null || contact.city == "") {
      return Container();
    }

    Widget street;
    if (contact.street != null && contact.street != "")  {
      street = Text(
        contact.street,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            contact.npa + " " + contact.city,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showWork() {
    if ((contact.jobTitle == null || contact.jobTitle == "") && (contact.company == null || contact.company == "")) {
      return Container();
    }

    Widget job;
    if (contact.jobTitle != null && contact.jobTitle != "")  {
      job = Text(
        contact.jobTitle,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      job = Container();
    }

    Widget company;
    if (contact.company != null && contact.company != "")  {
      company = Text(
        "At " + contact.company,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      company = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Work",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          job,
          company,
        ],
      ),
    );
  }
}