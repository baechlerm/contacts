import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:u_contact/data/model/Profile.dart';
import 'package:u_contact/ui/application/pages/ModifyProfilePage.dart';
import 'package:u_contact/ui/application/share/PersonContactWidget.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({this.profile});

  final Profile profile;

  @override
  _ProfileWidgetState createState() {
    return _ProfileWidgetState();
  }
}

class _ProfileWidgetState extends State<ProfileWidget> {
  Profile profile;

  @override
  void initState() {
    super.initState();

    profile = widget.profile;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          child: Padding(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: _showTitle(),
          ),
        ),
        Card(
          child: QrImage(
            data: profile.id != null ? profile.id : "",
            version: QrVersions.auto,
            size: 200.0,
          ),
        ),
        Card(
          child: Padding(
            padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
            child: Column(
              children: <Widget>[
                _showName(),
                _showPhone(),
                _showMail(),
                _showAdress(),
                _showDesciption(),
                _showSocial(),
                _showCompany(),
                _showJobTitle(),

                _showProPhone(),
                _showProMail(),
                PersonContactWidget(profileIds: profile.personsContact,),
                _showWebsite(),
                _showProAdress(),
                _showProDesciption(),
                _showProSocial(),

                _showCustoms(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _showTitle() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(
            profile.title,
            style: TextStyle(
              fontSize: 25,
            ),
          ),
        ),
        IconButton(
          icon: Icon(Icons.edit),
          iconSize: 25,
          onPressed: (){
            _onEdit();
          },
        ),
      ],
    );
  }

  Widget _showName() {
    if (profile.name == null || profile.name == "") {
      return Container();
    }
    String abr = (profile.name.substring(0,1) + profile.firstname.substring(0,1)).toUpperCase();
    String displayName = profile.name + " " + profile.firstname;

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _onEdit() async {
    await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ModifyProfilePage(editProfile: profile,)),
            );
    setState(() {
    });
  }

  Widget _showPhone() {
    if (profile.phoneNumber != null && profile.phoneNumber != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                profile.phoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + profile.phoneNumber);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + profile.phoneNumber);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showMail() {
    if (profile.mail != null && profile.mail != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                profile.mail,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + profile.mail);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  void _launchURL(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        print("Error url");
        print(url);
      }
    } catch (e) {
      print("Error with the emulator.");
    }
  }

  Widget _showAdress() {
    if (profile.npa == null || profile.npa == "" || profile.city == null || profile.city == "") {
      return Container();
    }

    Widget street;
    if (profile.street != null && profile.street != "")  {
      street = Text(
        profile.street,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            profile.npa + " " + profile.city,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showDesciption() {
    if (profile.personalDescription != null && profile.personalDescription != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    profile.personalDescription,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showCompany() {
    if (profile.companyName == null || profile.companyName == "") {
      return Container();
    }

    String abr = profile.companyName.substring(0,1);
    String displayName = profile.companyName;

    double littleCompany = 0;
    if (profile.name != null && profile.name != "") {
      littleCompany = 5;
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0 + littleCompany, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30 - littleCompany,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20 - littleCompany,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20 - littleCompany,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showJobTitle() {
    if (profile.jobTitle != null && profile.jobTitle != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Work",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    profile.jobTitle,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProPhone() {
    if (profile.jobPhoneNumber != null && profile.jobPhoneNumber != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                profile.jobPhoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + profile.jobPhoneNumber);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + profile.jobPhoneNumber);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProMail() {
    if (profile.jobMail != null && profile.jobMail != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                profile.jobMail,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + profile.jobMail);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showWebsite() {
    if (profile.website != null && profile.website.length > 0) {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                profile.website[0],
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.web,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL(profile.website[0]);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProAdress() {
    if (profile.companyNpa == null || profile.companyNpa == "" || profile.companyCity == null || profile.companyCity == "") {
      return Container();
    }

    Widget street;
    if (profile.companyStreet != null && profile.companyStreet != "")  {
      street = Text(
        profile.companyStreet,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            profile.companyNpa + " " + profile.companyCity,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showProDesciption() {
    if (profile.companyDescription != null && profile.companyDescription != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Company Description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    profile.companyDescription,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showSocial() {
    if (profile.name == null || profile.name == "") {
      return Container();
    }

    return _socialWidget();
  }

  Widget _showProSocial() {
    if (profile.name == null || profile.name == "") {
      return _socialWidget();
    }

    return Container();
  }

  Widget _socialWidget() {
    List<Widget> socials = List<Widget>();

    if (profile.facebook != null && profile.facebook != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.facebook),
            onPressed: () {
              _launchURL("https://www.facebook.com/"+profile.facebook);
            },
          )
      );
    }

    if (profile.twitter != null && profile.twitter != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.twitter),
            onPressed: () {
              _launchURL("https://twitter.com/"+profile.twitter);
            },
          )
      );
    }

    if (profile.snapchat != null && profile.snapchat != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.snapchat),
            onPressed: () {
              _launchURL("https://www.snapchat.com/add/"+profile.snapchat);
            },
          )
      );
    }

    if (profile.instagram != null && profile.instagram != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.instagram),
            onPressed: () {
              _launchURL("https://www.instagram.com/"+profile.instagram);
            },
          )
      );
    }

    if (profile.youtube != null && profile.youtube != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.youtube),
            onPressed: () {
              _launchURL("https://www.youtube.com/user/"+profile.youtube);
            },
          )
      );
    }

    if (profile.linkedin != null && profile.linkedin != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.linkedin),
            onPressed: () {
              _launchURL("https://www.linkedin.com/in/"+profile.linkedin);
            },
          )
      );
    }

    if (socials.length <= 0) {
      return Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: socials,
      ),
    );
  }

  Widget _showCustoms() {
    if (profile.customs == null || profile.customs.length <= 0) {
      return Container();
    }

    List<Widget> customs = List<Widget>();
    customs.add(
      Row(
        children: <Widget>[
          Text(
            "Other informations",
            style: TextStyle(
              decoration: TextDecoration.underline,
              fontSize: 17,
            ),
          ),
        ],
      ),
    );

    profile.customs.forEach((key, value) {
      customs.add(
        Row(
          children: <Widget>[
            Text(
              key.substring(key.indexOf("_") + 1) + ": " + value,
            ),
          ],
        ),
      );
    });

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        children: customs,
      ),
    );
  }
}