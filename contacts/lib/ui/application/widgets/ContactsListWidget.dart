import 'package:flutter/material.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/pages/AddLocalContactPage.dart';
import 'package:u_contact/ui/application/pages/ContactPage.dart';
import 'package:u_contact/ui/application/pages/LocalContactPage.dart';
import 'package:u_contact/ui/application/pages/ModifyLocalContactPage.dart';

class ContactsListWidget extends StatelessWidget {
  ContactsListWidget({this.contacts});

  final List<Contact> contacts;

  @override
  Widget build(BuildContext context) {
    contacts.sort((a, b) {
      String aName = "";
      String bName = "";

      if (a.profileId != null && a.profileId != "") {
        if (a.profile.name != null && a.profile.name != "") {
          aName = a.profile.name + " " + a.profile.firstname;
        } else {
          aName = a.profile.companyName;
        }
      } else {
        aName = a.name + " " + a.firstname;
      }

      if (b.profileId != null && b.profileId != "") {
        if (b.profile.name != null && b.profile.name != "") {
          bName = b.profile.name + " " + b.profile.firstname;
        } else {
          bName = b.profile.companyName;
        }
      } else {
        bName = b.name + " " + b.firstname;
      }

      return aName.toUpperCase().compareTo(bName.toUpperCase());
    });


    if (contacts.length > 0) {
      return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: contacts.length,
        itemBuilder: (BuildContext context, int index) {
          return _showCell(context, contacts[index]);
        },
      );
    }
     return Center(
        child: Text("No contact available."),
     );
  }

  Widget _showCell(BuildContext context, Contact contact) {
    return ListTile(
      onTap: () {
        if (contact.identifier != null && contact.identifier != "") {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => LocalContactPage(contact: contact)),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ContactPage(contact: contact)),
          );
        }
      },
      leading: _showAbr(contact),
      title: _showTitle(contact),
    );

  }

  Widget _showAbr(Contact contact) {
    String abr = "";

    if (contact.profileId != null && contact.profileId != "") {
      if (contact.profile.name != null && contact.profile.name.isNotEmpty) {
        abr = contact.profile.name.substring(0, 1) + contact.profile.firstname.substring(0, 1);
      } else if (contact.profile.companyName != null && contact.profile.companyName.isNotEmpty) {
        abr = contact.profile.companyName.substring(0, 1);
      }
    } else {
      abr = contact.name.substring(0, 1) + contact.firstname.substring(0, 1);
    }

    return CircleAvatar(
      child: Text(
        abr.toUpperCase(),
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
    );
  }

  Widget _showTitle(Contact contact) {
    String cellTitle = "";

    if (contact.profileId != null && contact.profileId != "") {
      if (contact.title != null && contact.title.isNotEmpty) {
        cellTitle = contact.title;
      } else if (contact.profile.name != null && contact.profile.name.isNotEmpty) {
        cellTitle = contact.profile.name + " " + contact.profile.firstname;
      } else {
        cellTitle = contact.profile.companyName;
      }
    } else {
      cellTitle = contact.name + " " + contact.firstname;
    }

    return Text(
      cellTitle,
    );
  }
}