import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/data/ContactLocal.dart';
import 'package:u_contact/data/model/Contact.dart';

import '../../../main.dart';

class LocalContactForm extends StatelessWidget {
  LocalContactForm({this.contact});

  final Contact contact;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    bool isNewContact = false;
    if (contact.identifier == null || contact.identifier == "") {
      isNewContact = true;
    }
    return Form(
      key: _formKey,
      child: Card(
        child: Column(
          children: <Widget>[
            _showLocalContact(),
            _showActions(context, isNewContact),
          ],
        ),
      ),
    );
  }

  Widget _showLocalContact() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.name,
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Name"),
            onSaved: (value) => contact.name = value.trim(),
            validator: (value) => value.isEmpty ? 'You should at least add a name' : null,
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.firstname,
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Firstname"),
            onSaved: (value) => contact.firstname = value.trim(),
            validator: (value) => value.isEmpty ? 'You should at least add a firstname' : null,
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.email,
            maxLines: 1,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Email"),
            onSaved: (value) => contact.email = value.trim(),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.phone,
            maxLines: 1,
            keyboardType: TextInputType.phone,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Phone number"),
            onSaved: (value) => contact.phone = value.trim(),
          ),
        ),
        Row(
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  initialValue: contact.npa,
                  maxLines: 1,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("Code"),
                  onSaved: (value) => contact.npa = value.trim(),
                ),
              ),
            ),
            Flexible(
              flex: 2,
              child: Container(
                padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
                child: TextFormField(
                  initialValue: contact.city,
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  decoration: MyApp.getInputDecoration("City"),
                  onSaved: (value) => contact.city = value.trim(),
                ),
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.street,
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Street"),
            onSaved: (value) => contact.street = value.trim(),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.company,
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Company name"),
            onSaved: (value) => contact.company = value.trim(),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(3, 5, 3, 5),
          child: TextFormField(
            initialValue: contact.jobTitle,
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: MyApp.getInputDecoration("Job title"),
            onSaved: (value) => contact.jobTitle = value.trim(),
          ),
        ),
      ],
    );
  }

  Widget _showActions(BuildContext context, bool isNewContact) {
    Widget deleteButton;
    Widget spaceBetweenContainer;

    if (isNewContact) {
      deleteButton = Container();
      spaceBetweenContainer = Container();
    } else {
      spaceBetweenContainer = Container(
        width: 10,
      );
      deleteButton = RaisedButton(
        child: Text("Delete"),
        onPressed: () {
          _onDelete(context);
        },
        color: Color.fromRGBO(0xe5, 0x73, 0x73, 1),
      );
    }


    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        deleteButton,
        spaceBetweenContainer,
        RaisedButton(
          child: Text("Save"),
          onPressed: () {
            _onSave(context, isNewContact);
          },
        ),
      ],
    );
  }

  void _onSave(BuildContext context, bool isNewContact) {
    var form = _formKey.currentState;

    if (form.validate()) {
      form.save();

      try {
        if (isNewContact) {
          ContactLocal().addLocalContact(contact).then((val) {
            ContactLocal().getLocalContacts().then((results) {
              BlocProvider.of<AccountBloc>(context).bloc.localContactsRetrieve(results);
            });
          });
        } else {
          ContactLocal().updateLocalContact(contact).then((val) {
            ContactLocal().getLocalContacts().then((results) {
              BlocProvider.of<AccountBloc>(context).bloc.localContactsRetrieve(results);
            });
          });
        }
        Navigator.pop(context);
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  void _onDelete(BuildContext context) {
    try {
      ContactLocal().deleteLocalContact(contact);
      ContactLocal().getLocalContacts().then((results) {
        BlocProvider.of<AccountBloc>(context).bloc.localContactsRetrieve(results);
      });
      Navigator.of(context).popUntil((route) => route.isFirst);
    } catch (e) {
      print('Error: $e');
    }
  }
}
