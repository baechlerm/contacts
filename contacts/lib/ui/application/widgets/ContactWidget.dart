import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:u_contact/data/model/Contact.dart';
import 'package:u_contact/ui/application/pages/ModifyContactPage.dart';
import 'package:u_contact/ui/application/share/PersonContactWidget.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactWidget extends StatefulWidget {
  ContactWidget({this.contact});

  final Contact contact;

  @override
  _ContactWidgetState createState() {
    return _ContactWidgetState();
  }
}

class _ContactWidgetState extends State<ContactWidget> {
  Contact contact;

  @override
  void initState() {
    super.initState();

    contact = widget.contact;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          child: Padding(
            padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
            child: Column(
              children: <Widget>[
                _showName(),
                _showPhone(),
                _showMail(),
                _showAdress(),
                _showDesciption(),
                _showSocial(),
                _showCompany(),
                _showJobTitle(),

                _showProPhone(),
                _showProMail(),
                PersonContactWidget(profileIds:contact.profile.personsContact,),
                _showWebsite(),
                _showProAdress(),
                _showProDesciption(),
                _showProSocial(),

                _showCustoms(),
                _showContactInfo(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _showContactInfo() {
    if ((contact.description == null || contact.description == "") && (contact.categories == null && contact.categories.length <= 0)) {
      return Container();
    }

    Widget category;
    if (contact.categories != null && contact.categories.length > 0) {
      List<Widget> categories = List<Widget>();
      categories.add(
        Row(
          children: <Widget>[
            Text(
              "Category",
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontSize: 17,
              ),
            ),
          ],
        ),
      );

      for (int i = 0; i < contact.categories.length; i++) {
        categories.add(
          Text(
            contact.categories[i],
            style: TextStyle(
              fontSize: 15,
            ),
          ),
        );
      }

      category = Padding(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: categories,
          ),
      );
    } else {
      category = Container();
    }

    Widget desc;
    if (contact.description != null && contact.description != "") {
      desc =  Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Contact description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    contact.description,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      desc = Container();
    }

    return Column(
      children: <Widget>[
        category,
        desc,
      ],
    );
  }

  Widget _showName() {
    if (contact.profile.name == null || contact.profile.name == "") {
      return Container();
    }
    String abr = (contact.profile.name.substring(0,1) + contact.profile.firstname.substring(0,1)).toUpperCase();
    String displayName = contact.profile.name + " " + contact.profile.firstname;

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.edit),
            iconSize: 25,
            onPressed: (){
              _onEdit();
            },
          ),
        ],
      ),
    );
  }

  Future<void> _onEdit() async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ModifyContactPage(contact: contact,)),
    );
    setState(() {
    });
  }

  Widget _showPhone() {
    if (contact.profile.phoneNumber != null && contact.profile.phoneNumber != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.profile.phoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + contact.profile.phoneNumber);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + contact.profile.phoneNumber);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showMail() {
    if (contact.profile.mail != null && contact.profile.mail != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.profile.mail,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + contact.profile.mail);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  void _launchURL(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        print("Error url");
        print(url);
      }
    } catch (e) {
      print("Error with the emulator.");
    }
  }

  Widget _showAdress() {
    if (contact.profile.npa == null || contact.profile.npa == "" || contact.profile.city == null || contact.profile.city == "") {
      return Container();
    }

    Widget street;
    if (contact.profile.street != null && contact.profile.street != "")  {
      street = Text(
        contact.profile.street,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            contact.profile.npa + " " + contact.profile.city,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showDesciption() {
    if (contact.profile.personalDescription != null && contact.profile.personalDescription != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    contact.profile.personalDescription,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showCompany() {
    if (contact.profile.companyName == null || contact.profile.companyName == "") {
      return Container();
    }

    String abr = contact.profile.companyName.substring(0,1);
    String displayName = contact.profile.companyName;

    double littleCompany = 0;
    if (contact.profile.name != null && contact.profile.name != "") {
      littleCompany = 5;
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 0 + littleCompany, 0, 10),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              radius: 30 - littleCompany,
              child: Text(
                abr.toUpperCase(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20 - littleCompany,
                  fontWeight: FontWeight.bold,
                ),
              ),
              backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
            ),
            Container(
              width: 10,
            ),
            Expanded(
              child: Text(
                displayName,
                style: TextStyle(
                  fontSize: 20 - littleCompany,
                ),
              ),
            ),
          ],
        ),
      );
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 30,
            child: Text(
              abr.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
          ),
          Container(
            width: 10,
          ),
          Expanded(
            child: Text(
              displayName,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.edit),
            iconSize: 25,
            onPressed: (){
              _onEdit();
            },
          ),
        ],
      ),
    );
  }

  Widget _showJobTitle() {
    if (contact.profile.jobTitle != null && contact.profile.jobTitle != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Work",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    contact.profile.jobTitle,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProPhone() {
    if (contact.profile.jobPhoneNumber != null && contact.profile.jobPhoneNumber != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.profile.jobPhoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.message,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("sms:" + contact.profile.jobPhoneNumber);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.phone,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("tel:" + contact.profile.jobPhoneNumber);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProMail() {
    if (contact.profile.jobMail != null && contact.profile.jobMail != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.profile.jobMail,
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.mail,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL("mailto:" + contact.profile.jobMail);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showWebsite() {
    if (contact.profile.website != null && contact.profile.website.length > 0) {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                contact.profile.website[0],
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.web,
                color: Color.fromRGBO(0x43, 0xa0, 0x47, 1),
              ),
              onPressed: () {
                _launchURL(contact.profile.website[0]);
              },
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showProAdress() {
    if (contact.profile.companyNpa == null || contact.profile.companyNpa == "" || contact.profile.companyCity == null || contact.profile.companyCity == "") {
      return Container();
    }

    Widget street;
    if (contact.profile.companyStreet != null && contact.profile.companyStreet != "")  {
      street = Text(
        contact.profile.companyStreet,
        style: TextStyle(
          fontSize: 15,
        ),
      );
    } else {
      street = Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Adress",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 17,
                ),
              ),
            ],
          ),
          Text(
            contact.profile.companyNpa + " " + contact.profile.companyCity,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          street,
        ],
      ),
    );
  }

  Widget _showProDesciption() {
    if (contact.profile.companyDescription != null && contact.profile.companyDescription != "") {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Company Description",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    contact.profile.companyDescription,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget _showSocial() {
    if (contact.profile.name == null || contact.profile.name == "") {
      return Container();
    }

    return _socialWidget();
  }

  Widget _showProSocial() {
    if (contact.profile.name == null || contact.profile.name == "") {
      return _socialWidget();
    }

    return Container();
  }

  Widget _socialWidget() {
    List<Widget> socials = List<Widget>();

    if (contact.profile.facebook != null && contact.profile.facebook != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.facebook),
            onPressed: () {
              _launchURL("https://www.facebook.com/"+contact.profile.facebook);
            },
          )
      );
    }

    if (contact.profile.twitter != null && contact.profile.twitter != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.twitter),
            onPressed: () {
              _launchURL("https://twitter.com/"+contact.profile.twitter);
              },
          )
      );
    }

    if (contact.profile.snapchat != null && contact.profile.snapchat != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.snapchat),
            onPressed: () {
              _launchURL("https://www.snapchat.com/add/"+contact.profile.snapchat);
              },
          )
      );
    }

    if (contact.profile.instagram != null && contact.profile.instagram != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.instagram),
            onPressed: () {
              _launchURL("https://www.instagram.com/"+contact.profile.instagram);
              },
          )
      );
    }

    if (contact.profile.youtube != null && contact.profile.youtube != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.youtube),
            onPressed: () {
              _launchURL("https://www.youtube.com/user/"+contact.profile.youtube);
              },
          )
      );
    }

    if (contact.profile.linkedin != null && contact.profile.linkedin != "") {
      socials.add(
          IconButton(
            icon: FaIcon(FontAwesomeIcons.linkedin),
            onPressed: () {
              _launchURL("https://www.linkedin.com/in/"+contact.profile.linkedin);
              },
          )
      );
    }

    if (socials.length <= 0) {
      return Container();
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: socials,
      ),
    );
  }

  Widget _showCustoms() {
    if (contact.profile.customs == null || contact.profile.customs.length <= 0) {
      return Container();
    }

    List<Widget> customs = List<Widget>();
    customs.add(
      Row(
        children: <Widget>[
          Text(
            "Other informations",
            style: TextStyle(
              decoration: TextDecoration.underline,
              fontSize: 17,
            ),
          ),
        ],
      ),
    );

    contact.profile.customs.forEach((key, value) {
      customs.add(
        Row(
          children: <Widget>[
            Text(
              key.substring(key.indexOf("_") + 1) + ": " + value,
            ),
          ],
        ),
      );
    });

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        children: customs,
      ),
    );
  }
}