import 'package:flutter/material.dart';
import 'package:u_contact/bloc/AccountBloc.dart';
import 'package:u_contact/bloc/BlocProvider.dart';
import 'package:u_contact/bloc/DatabaseProvider.dart';
import 'package:u_contact/data/model/User.dart';

import 'application/HomePage.dart';
import 'login/LoginPage.dart';

enum AuthState {
  WAITING,
  READY,
}

class RootPage extends StatefulWidget {
  _RootPageState createState() {
    return _RootPageState();
  }
}

class _RootPageState extends State<RootPage> {
  AuthState stateAuth = AuthState.WAITING;
  GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  bool _isInitialized = false;

  @override
  Widget build(BuildContext context) {
    //DatabaseProvider.of(context).data.signOut();
    if (!_isInitialized) {
      _isInitialized = true;
      checkConnection();
    }

    return StreamBuilder<User> (
      stream: BlocProvider.of<AccountBloc>(context).bloc.userStream,
      builder: (context, snapshot) {
        final user = snapshot.data;
        navigatorKey = GlobalKey();

        if (stateAuth == AuthState.WAITING) {
          return buildWaitingScreen();
        }

        if (user == null || user.mail == "") {
          return WillPopScope(
            onWillPop: () async => !await navigatorKey.currentState.maybePop(),

            child: Navigator(
              key: navigatorKey,
              onGenerateRoute: (RouteSettings settings) {
                return MaterialPageRoute(builder: (BuildContext _) => LoginPage(), settings: settings);
              },
            ),
          );
        }

        return WillPopScope(
          onWillPop: () async => !await navigatorKey.currentState.maybePop(),
          child: Navigator(
            key: navigatorKey,
            onGenerateRoute: (RouteSettings settings) {
              return MaterialPageRoute(builder: (BuildContext _) => HomePage(), settings: settings);
            },
          ),
        );
      },
    );
  }

  void checkConnection() {
    DatabaseProvider.of(context).data.getCurrentUser().then((user) {
      setState(() {
        stateAuth = AuthState.READY;
      });
      BlocProvider.of<AccountBloc>(context).bloc.userRetrieve(user);
    });
  }

  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }
}