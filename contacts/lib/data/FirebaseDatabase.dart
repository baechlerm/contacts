import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'model/Contact.dart';
import 'model/Profile.dart';
import 'model/User.dart';
import 'IDatabase.dart';
import 'dart:async';

class FirebaseDatabase implements IDatabase {
  static final FirebaseDatabase _accountBloc = FirebaseDatabase._internal();
  factory FirebaseDatabase() {
    return _accountBloc;
  }
  FirebaseDatabase._internal();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final Firestore    _firestore    = Firestore.instance;

  // ------------------------------
  // - Authentification -
  // ------------------------------
  Future<User> signIn(String email, String password) async {
    if (email == null || password == null) {
      return null;
    }
    email = email.toLowerCase();

    await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    return getCurrentUser();
  }

  Future<User> signUp(String email, String password, String name) async {
    if (email == null || password == null) {
      return null;
    }
    email = email.toLowerCase();

    await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    User user = User.createNewUser(email, name);
    await createUser(user);
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> forgetPassword(String email) async {
    if (email == null) {
      return null;
    }

    return _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  // ------------------------------
  // - Read datas -
  // ------------------------------
  Future<User> getCurrentUser() async {
    FirebaseUser fbUser = await _firebaseAuth.currentUser();

    User user;
    if (fbUser != null) {
      await _firestore.collection('users').document(fbUser.email).get().then((doc) {
        if (doc.data != null) {
          user = User.fromJson(doc.data);
          user.mail = doc.documentID;
        }
      });

      user.contacts = await getContacts(user.contacts);
    }
    return user;
  }

  Future<Profile> getProfile(String profileId) async {
    if (profileId == null || profileId == "") {
      return Profile();
    }

    Profile profile;

    await _firestore.collection('profiles').document(profileId).get().then((doc) {
      profile = Profile.fromJson(doc.data);
      profile.id = profileId;
    });

    return profile;
  }

  Future<List<Contact>> getContacts(List<Contact> contacts) async {
    if (contacts == null || contacts.isEmpty) {
      return List<Contact>();
    }

    List<DocumentSnapshot> docs = await Future.wait(contacts.map((p) => _firestore.collection('profiles').document(p.profileId).get()));

    for (int i = 0; i < docs.length; i++) {
      Profile profile = Profile.fromJson(docs[i].data);
      profile.id = docs[i].documentID;
      contacts[i].profile = profile;
    }

    return contacts;
  }

  Future<List<Profile>> getProfilesFromUser(User user) async {
    if (user == null || user.profiles.isEmpty) {
      return List<Profile>();
    }

    List<DocumentSnapshot> docs = await Future.wait(user.profiles.map((p) => _firestore.collection('profiles').document(p).get()));

    List<Profile> profiles = List<Profile>();
    for (var doc in docs) {
      Profile profile = Profile.fromJson(doc.data);
      profile.id = doc.documentID;
      profiles.add(profile);
    }

    return profiles;
  }

  // ------------------------------
  // - Create data -
  // ------------------------------
  Future<void> createUser(User user) async {
    if (user == null) {
      return null;
    }
    _firestore.collection('users').document(user.mail).setData(user.toJson());
  }

  Future<Contact> newContactFromUser(Contact contact, User user) async {
    if (user == null || contact == null) {
      return null;
    }

    user.contacts.add(contact);
    await updateUser(user);
    return contact;
  }

  Future<Profile> newProfileFromUser(Profile profile, User user) async {
    if (user == null || profile == null) {
      return null;
    }

    await _firestore.collection('profiles').add(profile.toJson()).then((result) {
      profile.id = result.documentID;
    });

    user.profiles.add(profile.id);
    await updateUser(user);
    return profile;
  }

  // ------------------------------
  // - Update data -
  // ------------------------------
  Future<void> updateUser(User user) async {
    if (user == null) {
      return null;
    }

    _firestore.collection('users').document(user.mail).updateData(user.toJson());
  }

  Future<void> updateProfile(Profile profile) async {
    if (profile == null) {
      return null;
    }

    _firestore.collection('profiles').document(profile.id).updateData(profile.toJson());
  }


  // ------------------------------
  // - Delete data -
  // ------------------------------
  Future<void> deleteUser(User user) async {
    if (user == null) {
      return null;
    }

    _firestore.collection('users').document(user.mail).delete();
  }

  Future<void> deleteContactFromUser(Contact contact, User user) async {
    if (user == null || contact == null) {
      return null;
    }

    user.contacts.remove(contact);
    await updateUser(user);
  }

  Future<void> deleteProfileFromUser(Profile profile, User user) async {
    if (user == null || profile == null) {
      return null;
    }

    user.profiles.remove(profile.id);
    await updateUser(user);
  }


  // ------------------------------
  // - Exists -
  // ------------------------------
  Future<bool> profileDoesExist(String profileId) async {
    bool doesExist = false;
    await _firestore.collection('profiles').document(profileId).get().then((doc) {
      doesExist = doc.exists;
    });

    return doesExist;
  }
}