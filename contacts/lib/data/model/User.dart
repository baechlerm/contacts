import 'Contact.dart';

class User {
  String name;
  String mail;
  List<String> categories = List<String>();

  // Link to datas
  List<String> profiles = List<String>();
  List<Contact> contacts = List<Contact>();

  // Constructor(s)
  User(this.mail);
  User.createNewUser(this.mail, this.name);
  User.fromJson(Map<String, dynamic> json) :
        this.name = json["name"] {
    this.categories = List<String>.from(json["categories"]);
    this.profiles = List<String>.from(json["profiles"]);

    for (Map<String, dynamic> contactJson in json["contacts"]) {
      this.contacts.add(Contact.fromJson(contactJson));
    }
  }

  // JSON
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map<String, dynamic>();
    List<Map<String, dynamic>> contactsJson = List<Map<String, dynamic>>();
    for (Contact contact in contacts) {
      contactsJson.add(contact.toJson());
    }

    json.addAll({
      "name": name,
      "categories": categories,
      "profiles": profiles,
      "contacts": contactsJson,
    });

    return json;
  }
}