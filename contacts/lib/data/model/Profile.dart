class Profile {
  String id;
  bool isPersonalProfile;
  String title;

  // Personal information
  String name = "";
  String firstname = "";
  String phoneNumber;
  String mail;
  String npa;
  String city;
  String street;
  String personalDescription;

  // Social network
  String facebook;
  String twitter;
  String snapchat;
  String instagram;
  String linkedin;
  String youtube;
  List<String> website = List<String>();

  // Work
  String companyProfile; // id of another card
  String companyName;
  String companyDescription;
  String companyNpa;
  String companyCity;
  String companyStreet;
  String jobTitle;
  String jobPhoneNumber;
  String jobMail;
  List<String> personsContact = List<String>();

  // Custom informations
  Map<String,String> customs = Map<String,String>();

  // Constructor(s)
  Profile();
  Profile.fromJson(Map<String, dynamic> json) :
        this.title = json["title"],
        this.isPersonalProfile = json["isPersonalProfile"],

        this.name = json["name"],
        this.firstname = json["firstname"],
        this.phoneNumber = json["phoneNumber"],
        this.mail = json["mail"],
        this.npa = json["adress"]["npa"],
        this.city = json["adress"]["city"],
        this.street = json["adress"]["street"],
        this.personalDescription = json["personalDescription"],

        this.facebook = json["facebook"],
        this.twitter = json["twitter"],
        this.snapchat = json["snapchat"],
        this.instagram = json["instagram"],
        this.linkedin = json["linkedin"],
        this.youtube = json["youtube"],

        this.companyProfile = json["companyProfile"],
        this.companyName = json["companyName"],
        this.companyDescription = json["companyDescription"],
        this.companyNpa = json["companyNpa"],
        this.companyCity = json["companyCity"],
        this.companyStreet = json["companyStreet"],
        this.jobTitle = json["jobTitle"],
        this.jobPhoneNumber = json["jobPhoneNumber"],
        this.jobMail = json["jobMail"] {
    this.website = List<String>.from(json["website"]);
    this.personsContact = List<String>.from(json["personsContact"]);

    this.customs = Map<String, String>.from(json["customs"]);
  }

  // JSON
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      "title": title,
      "isPersonalProfile": isPersonalProfile,

      "name": name,
      "firstname": firstname,
      "phoneNumber": phoneNumber,
      "mail": mail,
      "adress": {
        "npa": npa,
        "city": city,
        "street": street,
      },
      "personalDescription": personalDescription,

      "facebook": facebook,
      "twitter": twitter,
      "snapchat": snapchat,
      "instagram": instagram,
      "linkedin": linkedin,
      "youtube": youtube,
      "website": website,

      "companyProfile": companyProfile,
      "companyName": companyName,
      "companyDescription": companyDescription,
      "companyAdress": {
        "companyNpa": companyNpa,
        "companyCity": companyCity,
        "companyStreet": companyStreet,
      },
      "jobTitle": jobTitle,
      "jobPhoneNumber": jobPhoneNumber,
      "jobMail": jobMail,
      "personsContact": personsContact,

      "customs": customs,
    });

    return json;
  }
}