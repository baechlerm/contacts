import 'Profile.dart';

class Contact {
  String       title = "";
  String       description = "";
  List<String> categories = List<String>();

  // Link to data
  String       profileId;
  Profile      profile;

  // Local Contact
  String identifier;
  String name = "";
  String firstname = "";
  String email;
  String phone;
  String npa;
  String city;
  String street;
  String company;
  String jobTitle;

  // Constructor(s)
  Contact();
  Contact.fromProfile({this.profileId, this.profile});
  Contact.fromJson(Map<String, dynamic> json) :
        this.title = json["title"],
        this.description = json["description"],
        this.profileId = json["profileId"] {
    this.categories = List<String>.from(json["categories"] == null ? List<String>() : json["categories"]);
  }
  Contact.local({this.identifier, this.name, this.firstname, this.company, this.jobTitle, this.email, this.phone, this.npa, this.city, this.street});

  // JSON
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map<String, dynamic>();
    json.addAll({
      "title": title,
      "description": description,
      "categories": categories,
      "profileId": profileId,
    });

    return json;
  }

  bool isProfileDownloaded() {
    return profile != null;
  }
}