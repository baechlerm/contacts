import 'dart:async';

import 'model/Contact.dart';
import 'model/Profile.dart';
import 'model/User.dart';

abstract class IDatabase {
  Future<User> signIn(String email, String password);
  Future<User> signUp(String email, String password, String name);
  Future<void> signOut();
  Future<void> forgetPassword(String email);

  Future<User> getCurrentUser();
  Future<Profile> getProfile(String profileId);
  Future<List<Contact>> getContacts(List<Contact> contacts);
  Future<List<Profile>> getProfilesFromUser(User user);

  Future<void> createUser(User user);
  Future<Contact> newContactFromUser(Contact contact, User user);
  Future<Profile> newProfileFromUser(Profile profile, User user);

  Future<void> updateUser(User user);
  Future<void> updateProfile(Profile profile);

  Future<void> deleteContactFromUser(Contact contact, User user);
  Future<void> deleteProfileFromUser(Profile profile, User user);

  Future<bool> profileDoesExist(String profileId);
}