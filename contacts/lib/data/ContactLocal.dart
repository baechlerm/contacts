import 'package:contacts_service/contacts_service.dart' as local;
import 'model/Contact.dart';

class ContactLocal {
  static final ContactLocal _contactLocal = ContactLocal._internal();

  factory ContactLocal() {
    return _contactLocal;
  }
  ContactLocal._internal();

  Future<List<Contact>> getLocalContacts() async {
    Iterable<local.Contact> localContacts = await local.ContactsService.getContacts(withThumbnails: false);
    List<Contact> contacts = List<Contact>();
    
    if (localContacts == null || localContacts.isEmpty) {
      return contacts;
    }


    localContacts.forEach((localContact) {
      contacts.add(Contact.local(identifier: localContact.identifier, name: localContact.familyName, firstname: localContact.givenName, company: localContact.company, jobTitle: localContact.jobTitle,
          email: localContact.emails.isEmpty ? "" : localContact.emails.first.value, phone: localContact.phones.isEmpty ? "" : localContact.phones.first.value,
          npa: localContact.postalAddresses.isEmpty ? "" : localContact.postalAddresses.first.postcode, city: localContact.postalAddresses.isEmpty ? "" : localContact.postalAddresses.first.city,
          street: localContact.postalAddresses.isEmpty ? "" : localContact.postalAddresses.first.street));
    });

    return contacts;
  }

  Future<void> addLocalContact(Contact contact) async {
    local.Item email = local.Item(label: "Personal", value: contact.email);
    local.Item phone = local.Item(label: "Personal", value: contact.phone);
    local.PostalAddress address = local.PostalAddress(city: contact.city, street: contact.street, postcode: contact.npa);

    List<local.Item> emails = List<local.Item>();
    List<local.Item> phones = List<local.Item>();
    List<local.PostalAddress> addresses = List<local.PostalAddress>();
    emails.add(email);
    phones.add(phone);
    addresses.add(address);


    local.Contact localContact = local.Contact(givenName: contact.firstname, familyName: contact.name, company: contact.company, emails: emails,
    phones: phones, postalAddresses: addresses);

    await local.ContactsService.addContact(localContact);
    return null;
  }

  Future<void> updateLocalContact(Contact contact) async {
    local.Item email = local.Item(label: "Personal", value: contact.email);
    local.Item phone = local.Item(label: "Personal", value: contact.phone);
    local.PostalAddress address = local.PostalAddress(city: contact.city, street: contact.street, postcode: contact.npa);

    List<local.Item> emails = List<local.Item>();
    List<local.Item> phones = List<local.Item>();
    List<local.PostalAddress> addresses = List<local.PostalAddress>();
    emails.add(email);
    phones.add(phone);
    addresses.add(address);


    local.Contact localContact = local.Contact(givenName: contact.firstname, familyName: contact.name, company: contact.company, emails: emails,
        phones: phones, postalAddresses: addresses);

    localContact.identifier = contact.identifier;

    await local.ContactsService.updateContact(localContact);
    return null;
  }

  Future<void> deleteLocalContact(Contact contact) {
    local.Contact localContact = local.Contact();
    localContact.identifier = contact.identifier;
    local.ContactsService.deleteContact(localContact);
    return null;
  }
}